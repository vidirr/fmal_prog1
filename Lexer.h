#ifndef LEXER_H
#define LEXER_H
#include "Token.h"
using namespace std;

class Lexer {

	public:
		Lexer() {
			peeked = false;
		};
		Token nextToken();
		Token peek();

	private:
		Token myPeek;
		bool peeked;
};

#endif