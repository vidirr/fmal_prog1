#ifndef PARSER_H
#define PARSER_H
#include "Token.h"
#include "Lexer.h"

class Parser {

	public:
		Parser(Lexer l);
		void parse(); //Starts the parsing process.

	private:
		void lex(); //Retrieves the next token and stores in myToken.
		void expr(); //Calls term and then checks if we've got a PLUS token coming up, calling itself again if true.
		void term(); //Calls factor and then checks if we've got a MULT token coming up, calling itself again if true.
		void factor(); //Calls lex to get the next token, and then either returns if INT or processes if parenthesies.
		void error(); //Prints out error message end ends program with error.

		Lexer lexer;
		Token myToken;
};
#endif