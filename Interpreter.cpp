#include <stdlib.h>
#include <iostream>
#include <stack>
#include <cmath>
using namespace std;

void invalidInstruction(string e) {

	cout << "Interpreter error:\n\nInvalid instruction:" + e;
	exit(EXIT_FAILURE);
}

void toFewArguments(string e) {
	cout << "Interpreter error:\n\nToo few arguments for operator " + e;
	exit(EXIT_FAILURE);
}


class Interpreter {

	public:
		Interpreter();
		void fetch();
		void decode();
		void execute();
		bool drunk;

	private:
		string operation;
		stack<int> number_stack;
		int push_number;

};


Interpreter::Interpreter() {

	operation = "";
	//The number we pluck out of the PUSH # string.
	push_number = 0;
	//Used by main, we run the loop until this gets set to true.
	drunk = false;
}

void Interpreter::fetch() {

	//Bjaaaasic.
	if(cin) {
		getline(cin, operation, '\n');
	}
	else
		exit(EXIT_SUCCESS);
}

void Interpreter::decode() {

	//If we have ADD, MULT or PRINT.
	if(operation== "ADD" || operation== "MULT" || operation== "PRINT") {
		//We don't really have to do anything since execute uses the same operation string.
		return;
	}
	else if(operation.find("PUSH") == 0) {
		 //We have a PUSH # operation.
		//Get the number out of the string, and change the operation to something that the execute function understands.
		int lastDigit = operation.length() - 1;
		push_number = 0;

		//If we only get PUSH the operation is illegal.
		if(operation[lastDigit] == 'H') {
			invalidInstruction(operation);
		}

		//Parsing the integer part of PUSH #.
		for(int i = 0; isdigit(operation[lastDigit]); i++, lastDigit--) {
			
			push_number += (operation[lastDigit] - 48) * pow(10, i);

		}

		operation = "PUSH";
		return;
	}
	/* 
		If we get the empty string operation we return failure - why would we do that?
		I would call this an illegal operation.	
	*/
	else if(operation.length() == 0) {
		exit(EXIT_FAILURE);
	}
	/*
		We shouldn't reach this part, if we do something else was sent in.
	*/
	else
		invalidInstruction(operation);
}

void Interpreter::execute() {

	if(operation == "PUSH") {

		//Decode has already processed the input string, so we just push the number it plocked from the string onto the stack.
		number_stack.push(push_number);
	}

	else if(operation == "ADD" || operation == "MULT") {

		//Pop the stack 2 times, and then push the sum back onto the stack.
		int above_number = number_stack.top();
		number_stack.pop();
		/*
			If the stack is empty here we've recieved an invalid order/number of operations.
		*/
		if(number_stack.empty()) {
			toFewArguments(operation);
		}
		int below_number = number_stack.top();
		number_stack.pop();

		if(operation == "ADD") {
			number_stack.push(above_number + below_number);
			return;
		}
		else { //Operation is MULT
			number_stack.push(above_number * below_number);
			return;
		}
	}
	else if(operation == "PRINT") {

		if(number_stack.empty()) {
			cout << "Interpreter error:\nNothing to print.";
			drunk = true;
			return;
		}
		//Poppin' da stack man.
		cout << number_stack.top();
		number_stack.pop();
		drunk = true;
		return;
	}

}



int main() {

	Interpreter hjaltmonster = Interpreter();

	while(!hjaltmonster.drunk) {
	
		hjaltmonster.fetch();
		hjaltmonster.decode();
		hjaltmonster.execute();
	}

}