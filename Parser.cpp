#include "Parser.h"
#include "Lexer.h"
#include <iostream>
#include <stdlib.h>
#include <stack>
using namespace std;

Parser::Parser(Lexer l) {

	lexer = l;
}

void Parser::parse() {

	expr();
	lex();
	if(myToken.tCode == END) {
		cout << "PRINT";
		return;
	}
	else {
		error();
	}
}

void Parser::expr() {

	term();
	if(lexer.peek().tCode == PLUS) {

		lex();
		//We need to store the lexeme since myToken is a member variable, so we'll lose it otherwise.
		string storedLexeme = myToken.lexeme;
		
		if(lexer.peek().tCode == END) {
			error();
		}

		expr();
		cout << storedLexeme << endl;
	}


}

void Parser::term() {

	factor();

	if(lexer.peek().tCode == MULT) {
		lex();
		string storedLexeme = myToken.lexeme;

		if(lexer.peek().tCode == END) {
			error();
		}
		//If MULT then a term will follow.
		term();
		cout << storedLexeme << endl;	

	}
}

void Parser::factor() {

	lex();

	if(myToken.tCode == INT) {
		cout << myToken.lexeme << endl;
		return;
	}

	else if(myToken.tCode == LPAREN) {
		//If LPAREN then an expression will follow.
		expr();
		lex();
		//After processing expression we should have a RPAREN - if not the expression wasn't legal.
		if(myToken.tCode == RPAREN) {
			return;
		}
		else {
			error();
		}
	}
	//We might hit an endtoken here (after processing the last INT or RPAREN) - but if not then the expression wasn't legal.
	else if(myToken.tCode != END) {
		error();
	}
}

void Parser::error() {
	cout << "Syntax error!" << endl;
	return exit(EXIT_FAILURE);
}

void Parser::lex() {

	myToken = lexer.nextToken();
	if(myToken.tCode == ERROR) { 
		error();
	}
}