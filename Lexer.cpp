#include "Lexer.h"
#include <iostream>
using namespace std;

Token Lexer::nextToken() {

	Token theToken;
	char lexeme;

	//If we peeked that means we already have the token that nextToken 
	//should be retrieving, so we just return it and set peek to false.
	if(peeked) {
		peeked = false;
		return myPeek;
	}

	//Whilst we've got something to read in, we read it in.
	if(cin >> lexeme);
	else {
		//If there is nothing to read we've reached the end.
		theToken.lexeme = "END";
		theToken.tCode = END;
		return theToken;
	}

	//Check if the lexeme was an integer.
	if(isdigit(lexeme)) {
		//Push back to stdin and cin as an integer.
		cin.putback(lexeme);
		lexeme = cin.get();
		theToken.lexeme = "PUSH ";

		while(isdigit(lexeme)) {
			theToken.lexeme += lexeme;
			lexeme = cin.get();
		}
		//Put the character we checked but wasn't a number back onto stdin.
		cin.putback(lexeme);
		//Set the token and return it.
		theToken.tCode = INT;	
		return theToken;
	}


	switch(lexeme) {
		case '+':
			theToken.lexeme = "ADD";
			theToken.tCode = PLUS;
			break;
		case '*':
			theToken.lexeme = "MULT";
			theToken.tCode = MULT;
			break;
		case '(':
			theToken.lexeme = "LPAREN";
			theToken.tCode = LPAREN;
			break;
		case  ')':
			theToken.lexeme = "RPAREN";
			theToken.tCode = RPAREN;
			break;
		default:
			theToken.tCode = ERROR;
	}

	return theToken;
}

Token Lexer::peek() {

	//If we've already peeked and we're calling peek again, we just return the token.
	if(peeked) {
		return myPeek;
	}
	//If not we get the next token, but use peeked to make sure we don't lose the token.
	myPeek = nextToken();
	peeked = true;
	return myPeek;

}