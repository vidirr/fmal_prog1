#ifndef TOKEN_H
#define TOKEN_H
#include<string>
using namespace std;

enum TokenCode { INT, PLUS, MULT, LPAREN, RPAREN, ERROR, END };

class Token {

	public:
		Token() {lexeme = "";};
		string lexeme;
		TokenCode tCode;

};
#endif